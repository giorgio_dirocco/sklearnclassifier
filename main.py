import argparse
import os.path
import utils
from sklearn.model_selection import train_test_split, cross_validate, KFold
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report
from sklearn.metrics import precision_recall_fscore_support as prfs
import pandas as pd
import numpy as np

def main():

    #read input argument
    parser = argparse.ArgumentParser()
    parser.add_argument("-c","--classifier")
    parser.add_argument("-p","--dataFolder")
    parser.add_argument("-f","--dataFile")
    parser.add_argument("-t","--trainMethod")
    
    args = parser.parse_args()

    #check input value
    if( args.classifier != "tree" and args.classifier != "random-forest" and args.classifier != "svm" and args.classifier != "nn" and args.classifier != "xgboost" ):
        print("Invalid input classifier, VALID INPUT are: j48, random-forest, svm, nn, xgboost")
        return 0
    
    if( args.dataFolder == None ):
        workingDirectory = os.getcwd()
        print("Set {} as working directory".format(workingDirectory))
    else:
        workingDirectory = args.dataFolder
    
    if( args.dataFile == None ):
        dataFile = 'data.csv'
    else:
        dataFile = args.dataFile

    if( args.trainMethod == None):
        trainMethod = 'cross'
    else:
        trainMethod = args.trainMethod

    #read data file
    data = utils.readData(workingDirectory, dataFile, ';')

    #split data in input and label
    inputVector, label = utils.splitData(data)

    inputVector = utils.encodeCategorichalData(inputVector)

    #scaling features
    inputVector = utils.scaling(features=inputVector)

    #split data in train and test
    if(trainMethod == 'split'):
        x_train, x_test, y_train, y_test = train_test_split(inputVector, label, test_size=0.20, random_state=42)

        #initialize classifier
        model = utils.buildModel(args.classifier, x_train, y_train)

        #make predictions
        y_pred = model.predict(x_test)
        y_prob=model.predict_proba(x_test)
        #print("\nPrediction:\n", y_pred)

        #evaluate model
        accuracy = accuracy_score(y_test, y_pred)
        print("Accuracy: %.2f%%" % (accuracy * 100.0))
        confusionMatrix = confusion_matrix(y_test, y_pred)
        print("Confusion Matrix:\n", confusionMatrix)
        print(classification_report(y_test, y_pred, target_names=['Malato','Sano']))

    else:

        #get selected classifier
        model = utils.buildModel(classifierName=args.classifier,x_train=None,y_train=None,trained=False)
        
        #Kfold selection
        folds = 5
        cv = KFold(n_splits=folds,random_state=42,shuffle=False)
        
        precisions = []
        recalls    = []
        fscores    = []
        supports   = []

        utils.printProgressBar(0, folds, prefix='Progress CrossValidation', suffix='Complete', length=50)
        iter = 0
        for train_index, test_index in cv.split(inputVector):
           
            x_train, x_test, y_train, y_test = inputVector[train_index], inputVector[test_index], label[train_index], label[test_index]

            #train
            model.fit(x_train, y_train)

            #test
            y_pred=model.predict(x_test)
            y_prob=model.predict_proba(x_test)
            
            #eavaluation
            precision,recall,fscore,support = prfs(y_test,y_pred)
            
            #collect global evaluation
            precisions.append(precision)
            recalls.append(recall)
            fscores.append(fscore)
            supports.append(support)

            utils.printProgressBar(iter + 1, folds, prefix='Progress CrossValidation', suffix='Complete', length=50)
            iter += 1

        #organize results to print
        output = utils.organizeDataToPrintCV(precisions,recalls,fscores,supports)
        
        print("Cross Validation resutls:\n",output)

    return 0



if __name__ == "__main__":
    main()