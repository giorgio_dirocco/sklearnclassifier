import os.path
import sys
import pandas as pd
from xgboost import XGBClassifier
from sklearn.preprocessing import LabelEncoder, OneHotEncoder, StandardScaler, MinMaxScaler, RobustScaler, Normalizer
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn import svm
from sklearn.neural_network import MLPClassifier

def readData(folder=os.getcwd(), file="data.csv",delim=','):

    data = None 

    for root,dirs,files in os.walk(folder):
        if file in files:
            data = os.path.join(root,file)

    data = pd.read_csv(data,delimiter=delim)
    #print("I read: ", data)


    return data

def splitData(data):

        input = data.iloc[:,1:-1]
        label = data.iloc[:,-1]

        return input, label

def buildModel(classifierName, x_train, y_train,trained=True):

        if(classifierName == 'xgboost'):
                model = XGBClassifier()
        elif(classifierName == 'random-forest'):
                model = RandomForestClassifier(n_estimators=100, max_depth=20,random_state=7)
        elif(classifierName == 'tree'):
                model = DecisionTreeClassifier(random_state=7)
        elif (classifierName == 'svm'):
                model = svm.SVC(random_state=7)
        elif( classifierName == 'nn'):
                model = MLPClassifier(hidden_layer_sizes=(5,50),activation='relu',solver='lbfgs',random_state=1,shuffle=True)

        #train model
        if trained == True:
                model.fit(x_train,y_train)


        return model

def encodeCategorichalData(x):
        
        encoder1 = LabelEncoder()
        encoder2 = LabelEncoder()

        x['Sex'] = encoder1.fit_transform(x['Sex'])
        x['Work'] = encoder2.fit_transform(x['Work'])

        oneHotEnc = OneHotEncoder(categorical_features= [1])
        x = oneHotEnc.fit_transform(x).toarray()
        x = x[:,1:]

        return x

def scaling(features,method="robust"):

        scaler = None

        if method == 'robust':
                scaler = RobustScaler()
        elif method == 'minMax':
                scaler = MinMaxScaler()
        elif method == 'norm':
                scaler = Normalizer()
        elif method == 'standard':
                scaler = StandardScaler()
        

        return scaler.fit_transform(features)
       


def organizeDataToPrintCV(precisions,recalls,fscores,supports):

        resutl_df = pd.DataFrame(columns=['Pmin', 'Pmax', 'Pavg','Rmin', 'Rmax', 'Ravg','Fmin', 'Fmax', 'Favg','Smin', 'Smax', 'Savg'],index=['Malato', 'Sano','All'])
        pM = np.zeros(len(precisions))
        pS = np.zeros(len(precisions))
        rM = np.zeros(len(recalls))
        rS = np.zeros(len(recalls))
        fM = np.zeros(len(fscores))
        fS = np.zeros(len(fscores))
        sM = np.zeros(len(supports))
        sS = np.zeros(len(supports))
        
        cont = 0
        for pre,rec,fsc,supp in zip(precisions,recalls,fscores,supports):
            pM[cont] = pre[0]
            pS[cont] = pre[1]
            rM[cont] = rec[0]
            rS[cont] = rec[1] 
            sM[cont] = supp[0]
            sS[cont] = supp[1] 
            fM[cont] = fsc[0]
            fS[cont] = fsc[1]
            cont += 1

        resutl_df['Pmin']['Malato'] = pM.min()*100
        resutl_df['Rmin']['Malato'] = rM.min()*100
        resutl_df['Smin']['Malato'] = sM.min()
        resutl_df['Fmin']['Malato'] = fM.min()*100

        resutl_df['Pmin']['Sano'] = pS.min()*100
        resutl_df['Rmin']['Sano'] = rS.min()*100
        resutl_df['Smin']['Sano'] = sS.min()
        resutl_df['Fmin']['Sano'] = fS.min()*100

        resutl_df['Pmax']['Malato'] = pM.max()*100
        resutl_df['Rmax']['Malato'] = rM.max()*100
        resutl_df['Smax']['Malato'] = sM.max()
        resutl_df['Fmax']['Malato'] = fM.max()*100

        resutl_df['Pmax']['Sano'] = pS.max()*100
        resutl_df['Rmax']['Sano'] = rS.max()*100
        resutl_df['Smax']['Sano'] = sS.max()
        resutl_df['Fmax']['Sano'] = fS.max()*100

        resutl_df['Pavg']['Malato'] = pM.mean()*100
        resutl_df['Ravg']['Malato'] = rM.mean()*100
        resutl_df['Savg']['Malato'] = sM.mean()
        resutl_df['Favg']['Malato'] = fM.mean()*100

        resutl_df['Pavg']['Sano'] = pS.mean()*100
        resutl_df['Ravg']['Sano'] = rS.mean()*100
        resutl_df['Savg']['Sano'] = sS.mean()
        resutl_df['Favg']['Sano'] = fS.mean()*100

        resutl_df['Pmin']['All'] = (resutl_df['Pmin']['Malato'] + resutl_df['Pmin']['Sano']) / 2.0
        resutl_df['Pmax']['All'] = (resutl_df['Pmax']['Malato'] + resutl_df['Pmax']['Sano']) / 2.0
        resutl_df['Pavg']['All'] = (resutl_df['Pavg']['Malato'] + resutl_df['Pavg']['Sano']) / 2.0

        resutl_df['Rmin']['All'] = (resutl_df['Rmin']['Malato'] + resutl_df['Rmin']['Sano']) / 2.0
        resutl_df['Rmax']['All'] = (resutl_df['Rmax']['Malato'] + resutl_df['Rmax']['Sano']) / 2.0
        resutl_df['Ravg']['All'] = (resutl_df['Ravg']['Malato'] + resutl_df['Ravg']['Sano']) / 2.0

        resutl_df['Fmin']['All'] = (resutl_df['Fmin']['Malato'] + resutl_df['Fmin']['Sano']) / 2.0
        resutl_df['Fmax']['All'] = (resutl_df['Fmax']['Malato'] + resutl_df['Fmax']['Sano']) / 2.0
        resutl_df['Favg']['All'] = (resutl_df['Favg']['Malato'] + resutl_df['Favg']['Sano']) / 2.0

        resutl_df['Smin']['All'] = (resutl_df['Smin']['Malato'] + resutl_df['Smin']['Sano']) / 2.0
        resutl_df['Smax']['All'] = (resutl_df['Smax']['Malato'] + resutl_df['Smax']['Sano']) / 2.0
        resutl_df['Savg']['All'] = (resutl_df['Savg']['Malato'] + resutl_df['Savg']['Sano']) / 2.0

        return resutl_df


# Print iterations progress
def printProgressBar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█'):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end = '\r')
    # Print New Line on Complete
    if iteration == total: 
        print()